extends Node2D
class_name Weapon

enum STATE {
	READY,
	FIRING,
	RELOADING
}

@export var BULLET_SCENE: PackedScene

@onready var reload_timer = $ReloadTimer

var state: STATE = STATE.READY

func change_state(new_state: STATE):
	state = new_state
	
func fire():
	if state == STATE.FIRING || state == STATE.RELOADING:
		return
		
	change_state(STATE.FIRING)
	var bullet = BULLET_SCENE.instantiate()
	bullet.direction = Vector2.from_angle(global_rotation)
	bullet.global_position = global_position
	get_tree().root.add_child(bullet)
	change_state(STATE.RELOADING)
	reload_timer.start()
	
func _on_reload_timer_timeout():
	change_state(STATE.READY)
